This folder contains a basic example of embedding a Windows manifest
for testing the embedding project. The default manifest
enables UTF-8 support, and the program displays a message box with
UTF-8 encoded emoji, which will only work correctly if the manifest
is embedded in the executable.

To see how `rustc` runs the linker, set the `RUSTC_LOG` environment
variable to `rustc_codegen_ssa::back::link=info`.

To check whether the manifest was embedded correctly, use `mt.exe` from
the Visual Studio Build Tools:

    mt /inputresource:..\target\debug\test.exe /out:con /validate_manifest

To compile with a different toolchain once:

    cargo +stable-gnu build -v

or until it’s changed again:

    rustup default stable-gnu

To cross-compile for a different target:

    rustup target add x86_64-pc-windows-gnu
    cargo build -v --target x86_64-pc-windows-gnu

To check a manifest from a different target:

    mt /inputresource:..\target\x86_64-pc-windows-gnu\debug\test.exe /out:con /validate_manifest
