use std::{env, process::Command};

/// Build script for linking a manifest using windres.exe from
/// something like `mingw-w64-x86_64-gcc` and `mingw-w64-x86_64-binutils`.
fn main() {
    let mut object_file = env::var("OUT_DIR").unwrap();
    object_file.push_str("\\resource.o");
    let status = Command::new("windres")
        .args(["-i", "resource.rc", "-o", &object_file])
        .status();
    // No error if this fails, to keep IDE's happy.
    match status {
        Err(e) => println!("cargo:warning=windres: {}", e),
        Ok(s) if !s.success() => println!("cargo:warning=windres: {}", s),
        _ => {
            println!("cargo:rustc-link-arg-bins={}", object_file);
            println!("cargo:rerun-if-changed=cli.exe.manifest");
            println!("cargo:rerun-if-changed=resource.rc");
        }
    }
}
