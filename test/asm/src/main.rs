#![windows_subsystem = "windows"]

use std::ffi::c_void;
use std::ptr::null;

extern "system" {
    fn GetVersion() -> u32;
    fn MessageBoxA(hwnd: *const c_void, lpText: *const u8, lpCaption: *const u8, uType: u32) -> i32;
}

fn main() {
    let version = unsafe { GetVersion() };
    let major_version = version & 0x0f;
    let minor_version = (version & 0xf0) >> 8;
    let build = if version < 0x80000000 { version >> 16 } else { 0 };
    let message = format!(
        "It’s raining 🐈s and 🐕s.\n\nI think this is Windows {}.{} ({}).\0",
        major_version, minor_version, build
    );
    unsafe {
        MessageBoxA(null(), message.as_ptr(), "Manifest Test\0".as_ptr(), 0x40);
    }
}

std::arch::global_asm!(
    r#".section .rsrc,"dw""#,
    ".p2align 2",
    "2:",
    ".zero 14",
    ".short 1",
    ".long 24",
    ".long (3f - 2b) | 0x80000000",
    "3:",
    ".zero 14",
    ".short 1",
    ".long 1",
    ".long (4f - 2b) | 0x80000000",
    "4:",
    ".zero 14",
    ".short 1",
    ".long 1033",
    ".long 5f - 2b",
    "5:",
    ".long 6f@imgrel",
    ".long (7f - 6f)",
    ".zero 8",
    ".p2align 2",
    "6:",
    r#".ascii "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n""#,
    r#".ascii "<assembly xmlns=\"urn:schemas-microsoft-com:asm.v1\" manifestVersion=\"1.0\">\n""#,
    r#".ascii "  <compatibility xmlns=\"urn:schemas-microsoft-com:compatibility.v1\"><application><supportedOS Id=\"{{8e0f7a12-bfb3-4fe8-b9a5-48fd50a15a9a}}\"/></application></compatibility>\n""#,
    r#".ascii "  <application xmlns=\"urn:schemas-microsoft-com:asm.v3\"><windowsSettings><activeCodePage xmlns=\"http://schemas.microsoft.com/SMI/2019/WindowsSettings\">UTF-8</activeCodePage></windowsSettings></application>\n""#,
    r#".ascii "  <trustInfo xmlns=\"urn:schemas-microsoft-com:asm.v3\"><security><requestedPrivileges><requestedExecutionLevel level=\"asInvoker\" uiAccess=\"false\"/></requestedPrivileges></security></trustInfo>\n""#,
    r#".ascii "</assembly>""#,
    "7:",
);
